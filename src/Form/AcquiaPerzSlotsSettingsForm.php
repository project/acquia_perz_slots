<?php

declare(strict_types = 1);

namespace Drupal\acquia_perz_slots\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Acquia Personalization (Perz) Slots settings form.
 */
class AcquiaPerzSlotsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'acquia_perz_slots_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['acquia_perz_slots.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('acquia_perz_slots.settings');

    $form['slot_attributes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Slot attributes'),
      '#description' => $this->t('Enter one attribute per line, in the format <code>@code</code>.', ['@code' => 'attribute_name|attribute_value']),
      '#required' => TRUE,
      '#default_value' => $this->convertAssociativeArrayToString($config->get('slot_attributes')),
    ];
    $form['content_attributes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Content attributes'),
      '#description' => $this->t('Enter one attribute per line, in the format <code>@code</code>.', ['@code' => 'attribute_name|attribute_value']),
      '#default_value' => $this->convertAssociativeArrayToString($config->get('content_attributes')),
    ];
    $form['slot_entity_types'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Slot entity types'),
      '#description' => [
        'format' => ['#markup' => $this->t('Enter one entity type/bundle/mode per line, in the format <code>@entity_type_code</code> or <code>@entity_bundle_code</code> or <code>@entity_display_code</code>.', ['@entity_type_code' => 'entity_type', '@entity_display_code' => 'entity_type:bundle:mode'])],
      ],
      '#default_value' => $this->convertKeyedArrayToString($config->get('slot_entity_types')),
    ];
    $available_entity_types = $this->getAvailalbleEntityTypes();
    if ($available_entity_types) {
      $form['slot_entity_types']['#description']['details'] = [
        '#type' => 'details',
        '#title' => $this->t('Available slot entity types'),
        '#descriptions' => $this->t('Below are entity types that are pushed to Acquia Personalization.'),
        'items' => ['#markup' => implode('<br/>', $available_entity_types)],
      ];
    }
    $form['slot_entity_uuids'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Slot UUIDs'),
      '#description' => $this->t('Enter one UUID per line.'),
      '#default_value' => $this->convertKeyedArrayToString($config->get('slot_entity_uuids')),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $values['slot_attributes'] = $this->convertStringToAssociativeArray($values['slot_attributes']);
    $values['content_attributes'] = $this->convertStringToAssociativeArray($values['content_attributes']);
    $values['slot_entity_types'] = $this->convertStringToKeyedArray($values['slot_entity_types']);
    $values['slot_entity_uuids'] = $this->convertStringToKeyedArray($values['slot_entity_uuids']);
    $config = $this->config('acquia_perz_slots.settings');
    $config
      ->setData($values + $config->getRawData())
      ->save();

    // phpcs:disable: DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
    \Drupal::cache('render')->invalidateAll();
    if (\Drupal::moduleHandler()->moduleExists('page_cache')) {
      \Drupal::cache('page')->invalidateAll();
    }
    if (\Drupal::moduleHandler()->moduleExists('dynamic_page_cache')) {
      \Drupal::cache('dynamic_page_cache')->invalidateAll();
    }
    // phpcs:enable: DrupalPractice.Objects.GlobalDrupal.GlobalDrupal

    parent::submitForm($form, $form_state);
  }

  /**
   * Get available entity types.
   *
   * @return array
   *   An array of available entity types.
   */
  protected function getAvailalbleEntityTypes() {
    $available = [];
    $view_modes = $this->config('acquia_perz.entity_config')->get('view_modes') ?: [];
    foreach ($view_modes as $entity_type => $bundles) {
      $available[] = $entity_type;
      foreach ($bundles as $bundle => $view_modes) {
        $available[] = "$entity_type:$bundle";
        foreach (array_keys($view_modes) as $view_mode) {
          $available[] = "$entity_type:$bundle:$view_mode";
        }
      }
    }
    return $available;
  }
  /* ************************************************************************ */
  // Array to string conversion methods.
  /* ************************************************************************ */

  /**
   * Convert as keyed array to a string.
   *
   * @param array $array
   *   A keyed array.
   *
   * @return string
   *   The keyed array converted to a string.
   */
  protected function convertKeyedArrayToString(array $array): string {
    return ($array) ? implode(PHP_EOL, $array) : '';
  }

  /**
   * Convert an associative array to a string.
   *
   * @param array $array
   *   An associative array.
   *
   * @return string
   *   The associative array converted to a string.
   */
  protected function convertAssociativeArrayToString(array $array): string {
    $lines = [];
    foreach ($array as $key => $value) {
      $lines[] = ($value !== NULL) ? "$key|$value" : $key;
    }
    return implode(PHP_EOL, $lines);
  }

  /**
   * Convert string to an keyed array.
   *
   * @param string $string
   *   The raw string to convert into an keyed array.
   *
   * @return array
   *   A keyed array.
   */
  protected function convertStringToKeyedArray(string $string): array {
    $array = explode(PHP_EOL, $string);
    $array = array_map('trim', $array);
    $array = array_filter($array, 'strlen');
    return array_combine($array, $array);
  }

  /**
   * Convert string to an associative array.
   *
   * @param string $string
   *   The raw string to convert into an associative array.
   *
   * @return array
   *   An associative array.
   */
  protected function convertStringToAssociativeArray(string $string): array {
    $items = $this->convertStringToKeyedArray($string);
    $array = [];
    foreach ($items as $item) {
      $parts = explode('|', $item);
      $key = $parts[0];
      $value = $parts[1] ?? NULL;
      $array[trim($key)] = (!is_null($value)) ? trim($value) : $value;
    }
    return $array;
  }

}
