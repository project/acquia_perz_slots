<?php

declare(strict_types = 1);

namespace Drupal\acquia_perz_slots\EventSubscriber;

use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Response subscriber to handle Acquia Perz Slots in HTML responses.
 */
class AcquiaPerzSlotsHtmlResponseSubscriber implements EventSubscriberInterface {

  /**
   * Processes attachments for HtmlResponse responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond(ResponseEvent $event): void {
    $response = $event->getResponse();
    if (!$response instanceof HtmlResponse) {
      return;
    }

    $content = $response->getContent();
    if (!str_contains($content, '<!-- ACQUIA-PERZ-SLOT:')) {
      return;
    }

    // Convert <!-- ACQUIA-PERZ-SLOT: * --> comments into <div> tags.
    $content = preg_replace('#<!-- ACQUIA-PERZ-SLOT:(.+?)-->#', '<div\1>', $content);
    $content = preg_replace('#<!-- /ACQUIA-PERZ-SLOT:.+?-->#', '</div>', $content);

    $response->setContent($content);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

}
