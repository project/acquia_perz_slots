Table of contents
-----------------

- Introduction
- Approach
- Notes
- Installation
- Configuring


Introduction
------------

The Acquia Personalization Slots module adds a custom div slot wrapper to
selected rendered entities.


Approach
--------

Using comments as start and end tags ensures that the rendered entity that is
sent to Acquia Personalization does not have the <div> wrapper.

The start and end tag comments are converted to <div> tags when the entire
pages HTML is rendered with valid slots for Acquia Personalization.


Notes
-----

Using comments with simple string replacement is the most performant
and reliable way to manage slots.

Parsing the entire rendered DOM is not a reliable solution because any invalid
HTML would be altered or removed.


Installation
------------

Install the Acquia Personalization Slots  module as you would normally
[install a contributed Drupal module](https://www.drupal.org/node/1897420).


Configuration
-------------

- Review Acquia Personalization Slots configuration.
  (/admin/config/services/acquia-perz/slots)

