Steps for creating a new release
--------------------------------

  1. Review code
  3. Generate release notes
  4. Tag and create a new release
  5. Tag and create a hotfix release


1. Review code
--------------

    # Remove files that should never be reviewed.
    cd modules/sandbox/acquia_perz_slots
    rm *.patch interdiff-*

[PHP Linting](https://www.drupal.org/node/1587138)

    # Install drupal/coder (One time).
    composer global require drupal/coder

    # Update drupal/coder.
    composer global update drupal/coder

    # List coding standards.
    phpcs -i

    # Check Drupal PHP coding standards and best practices.
    phpcs .

    # Show sniff codes in all reports.
    phpcs -s .

    # Fix coding standards.
    phpcbf .

2. Generate release notes
-------------------------

[Git Release Notes for Drush](https://www.drupal.org/project/grn)

    drush release-notes --nouser 1.0.0-VERSION 1.0.x


3. Tag and create a new release
-------------------------------

[Tag a release](https://www.drupal.org/node/1066342)

    git checkout 1.0.x
    git up
    git tag 1.0.0-VERSION
    git push --tags
    git push origin tag 1.0.0-VERSION

[Create new release](https://www.drupal.org/node/add/project-release/2640714)


4. Tag and create a hotfix release
----------------------------------

    # Creete hotfix branch
    git checkout 1.0.LATEST-VERSION
    git checkout -b 1.0.NEXT-VERSION-hotfix
    git push -u origin 1.0.NEXT-VERSION-hotfix

    # Apply and commit remote patch
    curl https://www.drupal.org/files/issues/[project_name]-[issue-description]-[issue-number]-00.patch | git apply -
    git commit -am 'Issue #[issue-number]: [issue-description]'
    git push

    # Tag hotfix release.
    git tag 1.0.NEXT-VERSION
    git push --tags
    git push origin tag 1.0.NEXT-VERSION

    # Merge hotfix release with HEAD.
    git checkout 1.0.x
    git merge 1.0.NEXT-VERSION-hotfix

    # Delete hotfix release.
    git branch -D 1.0.NEXT-VERSION-hotfix
    git push origin :1.0.NEXT-VERSION-hotfix
