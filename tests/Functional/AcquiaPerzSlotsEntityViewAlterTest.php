<?php

declare(strict_types = 1);

namespace Drupal\Tests\acquia_perz_slots\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests for Acquia Personalization Slots entity view alter.
 *
 * @covers acquia_perz_slots_entity_view_alter()
 * @group acquia_perz_slots
 */
class AcquiaPerzSlotsEntityViewAlterTest extends BrowserTestBase {

  use NodeCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['acquia_perz_slots', 'node'];

  /**
   * Set default theme to stark.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests slots.
   */
  public function testSlots(): void {
    $assert_session = $this->assertSession();

    /* ********************************************************************** */

    $this->drupalCreateContentType(['type' => 'page']);
    $node = $this->drupalCreateNode();
    $uuid = $node->uuid();

    // Check that nodes are wrapped in a slot via default settings.
    $this->drupalGet($node->toUrl());
    $assert_session->responseContains('<div id="acquia-perz-slot-' . $uuid . '" class="acquia-perz-slot acquia-perz-slot-' . $uuid . ' acquia-perz-slot-node acquia-perz-slot-node-page" data-acquia-perz-slot="' . $uuid . '">');
    $assert_session->responseContains('<article class="acquia-perz-slot-content" role="article">');

    // Check that nodes are NOT wrapped in a slot.
    $this->setConfigValue('slot_entity_types', []);
    $this->drupalGet($node->toUrl());
    $assert_session->responseNotContains('<div id="acquia-perz-slot-' . $uuid . '" class="acquia-perz-slot acquia-perz-slot-' . $uuid . ' acquia-perz-slot-node acquia-perz-slot-node-page" data-acquia-perz-slot="' . $uuid . '">');
    $assert_session->responseNotContains('<article class="acquia-perz-slot-content" role="article">');

    // Check that entity_type:bundle are wrapped in a slot.
    $this->setConfigValue('slot_entity_types', ['node:page' => 'node:page']);
    $this->drupalGet($node->toUrl());
    $assert_session->responseContains('<div id="acquia-perz-slot-' . $uuid . '" class="acquia-perz-slot acquia-perz-slot-' . $uuid . ' acquia-perz-slot-node acquia-perz-slot-node-page" data-acquia-perz-slot="' . $uuid . '">');
    $assert_session->responseContains('<article class="acquia-perz-slot-content" role="article">');

    // Check that entity_type:bundle:mode are wrapped in a slot.
    $this->setConfigValue('slot_entity_types', ['node:page:default' => 'node:page:default']);
    $this->drupalGet($node->toUrl());
    $assert_session->responseContains('<div id="acquia-perz-slot-' . $uuid . '" class="acquia-perz-slot acquia-perz-slot-' . $uuid . ' acquia-perz-slot-node acquia-perz-slot-node-page" data-acquia-perz-slot="' . $uuid . '">');
    $assert_session->responseContains('<article class="acquia-perz-slot-content" role="article">');

    // Check that entity uuid are wrapped in a slot.
    $this->setConfigValue('slot_entity_types', []);
    $this->setConfigValue('slot_entity_uuids', [$node->uuid() => $node->uuid()]);
    $this->drupalGet($node->toUrl());
    $assert_session->responseContains('<div id="acquia-perz-slot-' . $uuid . '" class="acquia-perz-slot acquia-perz-slot-' . $uuid . ' acquia-perz-slot-node acquia-perz-slot-node-page" data-acquia-perz-slot="' . $uuid . '">');
    $assert_session->responseContains('<article class="acquia-perz-slot-content" role="article">');
  }

  /**
   * Set config setting value and clear cache.
   *
   * @param string $name
   *   Config setting name.
   * @param array $value
   *   Config setting value.
   */
  protected function setConfigValue(string $name, array $value): void {
    $this->config('acquia_perz_slots.settings')
      ->set($name, $value)
      ->save();

    \Drupal::cache('render')->invalidateAll();
    \Drupal::cache('page')->invalidateAll();
    if (\Drupal::moduleHandler()->moduleExists('dynamic_page_cache')) {
      \Drupal::cache('dynamic_page_cache')->invalidateAll();
    }

  }

}
