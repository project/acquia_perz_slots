<?php

declare(strict_types = 1);

namespace Drupal\Tests\acquia_perz_slots\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for Acquia Personalization Slots settings form.
 *
 * @covers \Drupal\acquia_perz_slots\Form\AcquiaPerzSlotsSettingsForm
 * @group acquia_perz_slots
 */
class AcquiaPerzSlotsSettingsFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['acquia_perz_slots'];

  /**
   * Set default theme to stark.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests setting form.
   */
  public function testSettingsForm(): void {
    $assert_session = $this->assertSession();

    $account = $this->createUser(['administer acquia perz']);
    $this->drupalLogin($account);

    /* ********************************************************************** */

    // Check updating the default config settings.
    $expected_data = $this->getConfigData();
    $this->drupalGet('/admin/config/services/acquia-perz/slots');
    $this->submitForm([], 'Save configuration');
    $actual_data = $this->getConfigData();
    $this->assertEquals($expected_data, $actual_data);

    // Check changing the config settings.
    $this->drupalGet('/admin/config/services/acquia-perz/slots');
    $edit = [
      'slot_attributes' => 'x|y',
      'content_attributes' => 'a|b',
      'slot_entity_types' => implode(PHP_EOL, ['a', 'b', 'c']),
      'slot_entity_uuids' => implode(PHP_EOL, ['d', 'e', 'f']),
    ];
    $this->submitForm($edit, 'Save configuration');
    $expected_data = [
      'slot_attributes' => ['x' => 'y'],
      'content_attributes' => ['a' => 'b'],
      'slot_entity_types' => [
        'a' => 'a',
        'b' => 'b',
        'c' => 'c',
      ],
      'slot_entity_uuids' => [
        'd' => 'd',
        'e' => 'e',
        'f' => 'f',
      ],
    ] + $expected_data;
    $actual_data = $this->getConfigData();
    $this->assertEquals($expected_data, $actual_data);
    foreach ($edit as $field => $value) {
      $assert_session->fieldValueEquals($field, $value);
    }

    // Check changing the config settings to unexpected string.
    $this->drupalGet('/admin/config/services/acquia-perz/slots');
    $edit = [
      'slot_attributes' => 'x',
      'content_attributes' => 'a',
      'slot_entity_types' => implode(PHP_EOL, ['a', 'b', 'c']),
      'slot_entity_uuids' => implode(PHP_EOL, ['d', 'e', 'f']),
    ];
    $this->submitForm($edit, 'Save configuration');
    $expected_data = [
        'slot_attributes' => ['x' => NULL],
        'content_attributes' => ['a' => NULL],
        'slot_entity_types' => [
          'a' => 'a',
          'b' => 'b',
          'c' => 'c',
        ],
        'slot_entity_uuids' => [
          'd' => 'd',
          'e' => 'e',
          'f' => 'f',
        ],
    ] + $expected_data;
    $actual_data = $this->getConfigData();
    $this->submitForm($edit, 'Save configuration');
    $this->assertEquals($expected_data, $actual_data);
    foreach ($edit as $field => $value) {
      $assert_session->fieldValueEquals($field, $value);
    }
  }

  /**
   * Get config settings data.
   *
   * @return array
   *   An associative array containing config settings raw data.
   */
  protected function getConfigData(): array {
    \Drupal::configFactory()->reset('acquia_perz_slots.settings');
    return $this
      ->config('acquia_perz_slots.settings')
      ->getRawData();
  }

}
